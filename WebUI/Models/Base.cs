﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.Models
{
    public class Base
    {
        public CreatedAt created_at { get; set; }
        public UpdatedAt updated_at { get; set; }
    }
    public class CreatedAt
    {
        public string _when { get; set; }
        public DateTime _date { get; set; }
    }

    public class UpdatedAt
    {
        public string _when { get; set; }
        public DateTime _date { get; set; }
    }
}
