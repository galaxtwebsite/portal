﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebUI.Models;
using Microsoft.AspNetCore.Http;

namespace WebUI.Controllers
{
    [Route("account")]
    public class AccountController : BaseController
    {
        [Route("")]
        [Route("index")]
        [Route("~/")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("login")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(User UserInput)
        {
            if (ModelState.IsValid)
            {
                User objUser = GetRequest<User>($"Users/{UserInput.id}").GetAwaiter().GetResult();

                if (objUser != null)
                {
                    if(objUser.password != UserInput.password)
                    {
                        ViewBag.error = "Invalid Account";
                        return View("Index");
                    }
                    HttpContext.Session.SetString("username", objUser.username);
                    HttpContext.Session.SetString("userid", objUser.id);
                    HttpContext.Session.SetString("vendor", objUser.vendor);
                    //return View("Dashboard");
                    return RedirectToAction("Index","Dashboard");
                }
                else
                {
                    ViewBag.error = "Invalid Account";
                    return View("Index");
                }
            }
            else
            {
                ViewBag.error = "Invalid Account";
                return View("Index");
            }
        }

        
    }
}
