﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebUI.Models;
using Microsoft.AspNetCore.Http;

namespace WebUI.Controllers
{
    [Route("dashboard")]
    public class DashboardController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        [Route("logout")]
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("username");
            HttpContext.Session.Remove("userid");
            HttpContext.Session.Remove("vendor");
            return RedirectToAction("Index", "Account");
        }
    }
}
